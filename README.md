# Disenchanting

Disenchanting is a Forge mod for Minecraft 1.14 and 1.15. It is my very first working Minecraft mod! It is based on the Fabric mod [Disenchanting](https://www.curseforge.com/minecraft/mc-mods/disenchanting).

This mod adds a new block, the Disenchanter, which let's you transfer enchantments from enchanted items onto books. Currently there is no experience cost nor any configurations.

# Download

[Download on CurseForge](https://www.curseforge.com/minecraft/mc-mods/disenchantingforge)

# Recipe
- 1 Anvil
- 1 Enchanting table
- 2 Gold ingots
- 3 Obsidian blocks

![alt text](web/recipe.png "Disenchanter recipe")

# How to use
1. Put an enchanted item into the left slot
2. Place a book or stack of books into the middle slot
3. Enchanted book will generate with the first enchantment from the enchanted item
4. Taking the book will remove that enchantment from the item and give you the book

# Notes
- Shift-clicking the enchanted book in the output slot will try to remove ALL enchantments from the item if enough books are available, and will consume ALL necessary experience if that is configured.
- Config options are set at the server level, so any client-side values are ignored when playing on a server.

# Config options
- requires_xp: Does disenchanting require experience?
- xp_cost: Relative scale (1 - 10) of how much XP the disenchanter costs to use (lower requires less XP)
- random_enchantment: When true, the disenchanter will pick a random enchantment from the item. Default value is false, which is existing behavior.

# Known bugs
- None

# BDCraft Packs
- [Disenchanting 128x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-128x.zip?inline=false)
- [Disenchanting 256x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-256x.zip?inline=false)
- [Disenchanting 512x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-512x.zip?inline=false)

# Roadmap
- ~~Add a configurable experience cost~~ (#6)
- ~~Port to 1.15.2~~ (#10)
- ~~Make a Sphaxified resource pack~~ (#8)
- Make a sound when disenchanting (#5)
- Enhance 3D model that highlights the anvil (#7)

# Special thanks
- [McJty](https://github.com/McJtyMods) for his Forge modding tutorial for 1.14
- [Tsudico](https://github.com/Tsudico/Disenchanting) for his Disenchanting mod for Fabric
- [akharding91](https://gitlab.com/akharding91) for the BDCraft textures
